#ifndef DRAWING_H
#define DRAWING_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <QPen>
#include <QBrush>
#include <QColor>

using namespace std;


struct Point
{
	double x;
	double y;

	double pressure;
	int label_;
	double interval; //time indicator

	Point(double argx, double argy, double argPressure = 0, double argInterval = 0, int label = -1)
        :x(argx), y(argy), pressure(argPressure), label_(label), interval(argInterval) {}

    ~Point(){}
};

class Segment
{
public:
	int pointBeginIndex;
	int pointEndIndex;
	int userStrokeIndex;
	int label;

	Segment(int pointBeginIndexArg = 0, int pointEndIndexArg = 0, int userStrokeIndexArg = 0, int label = -1) :
		pointBeginIndex(pointBeginIndexArg),
		pointEndIndex(pointEndIndexArg),
		userStrokeIndex(userStrokeIndexArg), 
        label (label){}

    ~Segment(){}
};

struct Stroke
{
public:
	std::vector< Point* > points;
	int index;
	int first_segment_ind;
	int last_segment_ind;

	int labelSemantic;
	bool is_removed_;

	Stroke(): 
		index(0), 
		first_segment_ind(0), 
		last_segment_ind(0),
		labelSemantic(0),
		is_removed_(false)
	{};

	~Stroke() {
		for (int i = 0; i < points.size(); i++)
		{
			delete points[i];
		}
	};
};

class UserDrawing
{
private:

	int num_clusters;

	//Properties of the strokes
	QColor m_color;
	QBrush m_brush;
	double pen_width;
	QPen m_pen;

public:

	int canvas_width;
	int canvas_height;
	int cur_label_;
	string name;

	std::vector< Stroke* > userStrokes;
	std::vector< Segment* > strokeSegments;

	UserDrawing() : 
		name("drawing.png")
		, m_color(QColor(0, 0, 0))
		, m_brush(m_color)
		, pen_width(1.5)
		, m_pen(m_brush, pen_width, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin)
		{}
	~UserDrawing();

	void setColor(QColor c);
	
	void displayDrawing(QPixmap &label_image);

};



#endif
