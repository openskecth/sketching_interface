#include "drawing.h"
#include <QtGui>


using namespace std;


UserDrawing::~UserDrawing()
{
	for (int i = 0; i < userStrokes.size(); i++)
	{
		delete userStrokes[i];
	}
	for (int i = 0; i < strokeSegments.size(); i++)
	{
		delete strokeSegments[i];
	}
}

void UserDrawing::setColor(QColor c)
{
	m_color = c;
}





void UserDrawing::displayDrawing(QPixmap &sketch_pixmap)
{
	QPainter painter(&sketch_pixmap);
	painter.setRenderHint(QPainter::Antialiasing);
	
	QPointF lastPoint;
	QPointF newPoint;
	
	double p;

	for (int i = 0; i < userStrokes.size(); i++)
	{
		if (userStrokes[i]->points.size() < 2)
		{
			continue;
		}

		for (int j = 0; j < userStrokes[i]->points.size()-1; j++)
		{
			if (userStrokes[i]->is_removed_)
				continue;

			/*p = userStrokes[i]->points[j + 1]->pressure;
			m_color.setAlphaF(max(p*p, 0.3));
			m_pen.setWidthF(p * pen_width);
			m_pen.setColor(m_color);*/

			p = userStrokes[i]->points[j+1]->pressure;
			
			m_pen.setWidthF(p * pen_width);
			m_color.setAlphaF(p);
			m_pen.setColor(m_color);

			lastPoint = QPointF(userStrokes[i]->points[j]->x, userStrokes[i]->points[j]->y);
			newPoint = QPointF(userStrokes[i]->points[j+1]->x, userStrokes[i]->points[j+1]->y);
			painter.setPen(m_pen);
			painter.drawLine(lastPoint, newPoint);
		}
	}
	painter.end();
};
