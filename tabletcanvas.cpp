#include <QtWidgets>
#include <math.h>

#include "tabletcanvas.h"

#include <ctime>
#include <iomanip>

using namespace std;

TabletCanvas::TabletCanvas(int height, int width, QColor bg_color)
: QWidget(Q_NULLPTR)
, m_alphaChannelValuator(TangentialPressureValuator)
, m_colorSaturationValuator(NoValuator)
, m_lineWidthValuator(PressureValuator)
, m_color(QColor(0, 0, 0))
, m_brush(m_color)
, pen_width(1.5)
, m_pen(m_brush, pen_width, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin)//Qt::RoundCap, Qt::RoundJoin)
, m_deviceDown(false)
, stroke_index(0)
, bg_color_(bg_color)
{
	drawing = new UserDrawing();
	m_height = height;
	m_width = width;

	//Resize the widget window:
	resize(m_width, m_height);
	initPixmap();
	setAutoFillBackground(false);

	//time(&startTime);
	startTime = clock();
	currTime = 0;
	
	QCursor cursor = QCursor(QPixmap(":/cursor/cross.png"));
	
	setCursor(cursor);
}


TabletCanvas::~TabletCanvas()
{
	
	delete drawing;
	
}

void TabletCanvas::clear()
{
	resize(m_width, m_height);
	//initPixmap();
	//setAutoFillBackground(true);

	startTime = clock();
	currTime = clock();

	delete drawing;
}


void TabletCanvas::serialize(QString filename)
{
	QJsonObject root;

	QJsonObject canvasObject;

	canvasObject.insert("pen_width", QJsonValue::fromVariant(pen_width));
	canvasObject.insert("width", QJsonValue::fromVariant(m_width));
	canvasObject.insert("height", QJsonValue::fromVariant(m_height));
	root.insert("canvas", canvasObject);

	QJsonArray strokesArray;
	int n_strokes = 0;

	for (int i = 0; i < drawing->userStrokes.size(); i++)
	{

		if (drawing->userStrokes[i]->points.size() == 0)
		{
			continue;
		}
		QJsonObject strokeObject;

		QJsonArray pointsArray;

		for (int j = 0; j < drawing->userStrokes[i]->points.size(); j++)
		{
			QJsonObject pointObject;

			pointObject.insert("x", QJsonValue::fromVariant(drawing->userStrokes[i]->points[j]->x));
			pointObject.insert("y", QJsonValue::fromVariant(drawing->userStrokes[i]->points[j]->y));
			pointObject.insert("p", QJsonValue::fromVariant(drawing->userStrokes[i]->points[j]->pressure));
			pointObject.insert("t", QJsonValue::fromVariant(drawing->userStrokes[i]->points[j]->interval));

			pointsArray.push_back(pointObject);
		}

		//strokesArray.push_back(pointsArray);
		strokeObject.insert("points", pointsArray);
		strokeObject.insert("is_removed", QJsonValue::fromVariant(drawing->userStrokes[i]->is_removed_));
		
		strokesArray.push_back(strokeObject);

		n_strokes++;
	}

	root.insert("strokes", strokesArray);

	QJsonDocument doc(root);
	QFile jsonFile;
	jsonFile.setFileName(filename);
	jsonFile.open(QIODevice::WriteOnly | QIODevice::Text);
	jsonFile.write(doc.toJson());
	jsonFile.close();
}



//void TabletCanvas::play()
//{
//
//	for (int i = 0; i < drawing->userStrokes.size(); i++)
//	{
//		lastPoint.pos = QPointF(drawing->userStrokes[i]->points[0]->x, drawing->userStrokes[i]->points[0]->y);
//		lastPoint.rotation = 1.0;
//
//		//TODO:: finish code here
//	}
//}

void TabletCanvas::initPixmap()
{
	QPixmap newPixmap = QPixmap(m_width, m_height);
	newPixmap.fill(bg_color_);
	QPainter painter(&newPixmap);
	if (!m_pixmap.isNull())
		painter.drawPixmap(0, 0, m_pixmap);
	painter.end();
	m_pixmap = newPixmap;
}



bool TabletCanvas::saveImage(const QString &file)
{
	bool success = m_pixmap.save(file);

	if (success)
	{
		QString dataFile(file);
		dataFile.replace(QString(".png"), QString(".json"));
		serialize(dataFile);
		return true;
	}
	
	return false;
}

void TabletCanvas::displayPixmap(const QPixmap pixmap)
{		
	m_pixmap = pixmap;

	this->m_width  = m_pixmap.width();
	this->m_height = m_pixmap.height();

	resize(m_pixmap.width(), m_pixmap.height());
	update();
}

void TabletCanvas::tabletEvent(QTabletEvent *event)
{
//	cout << "tabletEvent" << endl;
	switch (event->type()) {
	case QEvent::TabletPress:
		if (!m_deviceDown) {
			m_deviceDown = true;
			lastPoint.pos = event->posF();
			lastPoint.rotation = event->rotation();

			//Create new stroke.
			Stroke *stroke = new Stroke();
			drawing->userStrokes.push_back(stroke);
			drawing->userStrokes.back()->index = stroke_index++;
		}
		break;
	case QEvent::TabletMove:
		if (event->device() == QTabletEvent::RotationStylus)
			updateCursor(event);
		if (m_deviceDown) {
			updateBrush(event);
			QPainter painter(&m_pixmap);
			paintPixmap(painter, event);
			
			lastPoint.pos = event->posF();
			lastPoint.rotation = event->rotation();

			currTime = clock();
			double timeElapsed = float(currTime - startTime) / double(CLOCKS_PER_SEC); // in seconds * 1000 ; // in miliseconds

			//Create new point.
			Point* point = new Point(lastPoint.pos.x(),
				lastPoint.pos.y(),
				event->pressure(),
				timeElapsed,
				drawing->cur_label_);
			
			drawing->userStrokes.back()->points.push_back(point);
		}
		break;
	case QEvent::TabletRelease:
		if (m_deviceDown && event->buttons() == Qt::NoButton)
			m_deviceDown = false;
		break;
	default:
		break;
	}
	event->accept();
	update();
}

void TabletCanvas::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.drawPixmap(0, 0, m_pixmap);
}

/*
In this function we draw on the pixmap based on the movement of the tool.If the tool used on the 
tablet is a stylus, we want to draw a line from the last - known position to the current position.
We also assume that this is a reasonable handling of any unknown device, but update the status bar 
with a warning.
*/
void TabletCanvas::paintPixmap(QPainter &painter, QTabletEvent *event)
{
	painter.setRenderHint(QPainter::Antialiasing);
	//cout << event->device() << endl;
	//cout << "paintPixmap" << endl;

	switch (event->device()) {
		case QTabletEvent::Airbrush:
		{
		}
		break;
		case QTabletEvent::RotationStylus:
		{
		}
		break;
		case QTabletEvent::Puck:
		case QTabletEvent::FourDMouse:
		{
			const QString error(tr("This input device is not supported by the example."));
			#ifndef QT_NO_STATUSTIP
												 QStatusTipEvent status(error);
												 QApplication::sendEvent(this, &status);
			#else
												 qWarning() << error;
			#endif
		}
		break;
		default:
		{
			   const QString error(tr("Unknown tablet device - treating as stylus"));
				#ifndef QT_NO_STATUSTIP
							   QStatusTipEvent status(error);
							   QApplication::sendEvent(this, &status);
				#else
							   qWarning() << error;
				#endif
		}
		// FALL-THROUGH
		case QTabletEvent::Stylus:
			painter.setPen(m_pen);

			switch (event->type()) {
				/*case QEvent::TabletPress:
				{
					
					path.moveTo(event->posF());
				}
				break;*/
				case QEvent::TabletMove:
					{  
					/*	   path = QPainterPath();
						   path.moveTo(lastPoint.pos);
						   path.lineTo(event->posF());
						   painter.drawPath(path);*/
						painter.drawLine(lastPoint.pos, event->posF());
					}
				break;
			}

		break;
	}
}

void TabletCanvas::updateBrush(const QTabletEvent *event)
{
	/*m_color.setAlpha(255);
	m_pen.setWidthF(event->pressure() * pen_width);	

	if (event->pointerType() == QTabletEvent::Eraser) {
		m_brush.setColor(Qt::white);
		m_pen.setColor(Qt::white);
		m_pen.setWidthF(0);
	}
	else {
		m_brush.setColor(m_color);
		m_pen.setColor(m_color);
	}
*/
	//double t = event->pressure()*event->pressure();

	/*int c = 0.5* (1 - t) * 255.0;
	m_color = QColor(c, c, c);*/
    //m_color = QColor(0, 0, 0);
	//m_color.setAlphaF(max(event->pressure()*event->pressure(), 0.2));

	
	//0 represents a fully transparent color, while 255 represents a fully opaque color

	//m_color.setAlphaF(max(event->pressure()*event->pressure(), 0.3));
	m_color.setAlphaF(event->pressure());
	//m_color.setAlphaF(1.0);
	//m_pen.setWidthF(event->pressure()* event->pressure() * pen_width);
	m_pen.setWidthF(event->pressure()* pen_width);

	if (event->pointerType() == QTabletEvent::Eraser) {
		m_brush.setColor(Qt::white);
		m_pen.setColor(Qt::white);
		m_pen.setWidthF(0);
	}
	else {
		m_brush.setColor(m_color);
		m_pen.setColor(m_color);
	}


}

void TabletCanvas::updateCursor(const QTabletEvent *event)
{
	QCursor cursor;
	if (event->type() != QEvent::TabletLeaveProximity) {
		if (event->pointerType() == QTabletEvent::Eraser) {
			//cursor = QCursor(QPixmap(":/images/cursor-eraser.png"), 3, 28);
		}
		else {
			switch (event->device()) {
				case QTabletEvent::Stylus:
					cursor = QCursor(QPixmap(":/cursor/cross.png"), 0, 0);
					break;

				default:
					break;
			}
		}
	}
	setCursor(cursor);
}

//QPixmap TabletCanvas::getPixmapTransparent(double alpha)
//{
//	
//	QImage image(m_pixmap.width(), m_pixmap.height(), QImage::Format_ARGB32_Premultiplied);			
//	
//	image.fill(Qt::transparent);
//	//image.fill(Qt::transparent);
//	QPainter p(&image);
//	p.setCompositionMode(QPainter::CompositionMode_Source);
//	p.setOpacity(alpha);
//	p.drawPixmap(0, 0, m_pixmap);
//	p.end();
//
//	return QPixmap::fromImage(image);
//	//update();
//
//	
//	//QPixmap output(m_pixmap.size());
//	//output.fill(Qt::transparent);
//	//
//	//QPainter p(&output);
//	//p.setOpacity(alpha);
//	//p.drawPixmap(0, 0, m_pixmap);
//	//p.end();
//	//m_pixmap = output;
//}

void TabletCanvas::resizeEvent(QResizeEvent *)
{
	initPixmap();
}
