#ifndef DRAWINGPAGE2_H
#define DRAWINGPAGE2_H

#include <QDialog>
#include <QtWidgets>
#include <string>
#include "tabletcanvas.h"

using namespace std;

class QGroupBox;
class QAction;
class QSlider;

class DrawingPage2:
	public QDialog
{
	Q_OBJECT

public:
	DrawingPage2(int screen_number, const char *hand, string drawings_dir = "./drawings/", string system = "windows");
	~DrawingPage2();

	void createLayout();

protected:
	string system_;

	string view_;
	string folder_drawings_;
	string folder_views_;
	string fileVersion_;

	QPixmap initial_drawing_pixmap_;
	int hand_; //1 right, 0 left
	const char *task_;

	int screen_width;
	int screen_height;
	int canvas_dim;
	int img_w;

	Qt::AlignmentFlag flag_imgs;
	Qt::AlignmentFlag flag_canvas;

	QSlider *transparency_slider;
	QGroupBox *gridGroupBox;
	QGridLayout *layout;
	QVBoxLayout *mainLayout;

	//Buttons
	QHBoxLayout *qhbox_layout;
	QGroupBox *groupBttns;

	QPushButton* completed;
	QPushButton* completedCleanBtn;
	QPushButton* undoBttn;


	//Reference Images
	QPixmap orth_views_;
	QLabel *orth_images_slot_;
	//QLabel *front_image_text_;
	//QLabel *top_image_text_;
	//QLabel *side_image_text_;

	TabletCanvas *orth_drawing; //canavas to store measurements


	//Page 2
	QLabel *instruction;

	QLabel *canvasTaskText;
	TabletCanvas *initial_drawing;
	TabletCanvas *clean_drawing;

	QLabel *imageBg;
	

	void saveInitialDrawing();
	void setAlignment(); //define the alignment given hand prefernces
	void createReferenceViews();
	void createButtonsIntial();
	void deleteButtonsIntial();
	void createButtonsClean(double transparency);
	void removeReferenceViews();

	QPixmap getPixmapTransparent(const QPixmap input, double alpha);
	
	void recordScreenData(int screen_number, const char *hand);


	protected slots:
		void handleNextTaskBttn();
		void changeTransparency(int alpha);
		void saveCleanDrawing();
		void done();
		void undoInitial();
		void undoClean();

};

inline bool file_exists(const std::string& name);

//drawing viewpoint 1
class DrawingPage2V1 : public DrawingPage2
{
public:
	DrawingPage2V1(int screen_number = 0, const char *hand = "right");
};

//drawing viewpoint 2
class DrawingPage2V2 : public DrawingPage2
{
public:
	DrawingPage2V2(int screen_number = 0, const char *hand = "right");
};


#endif
