#include "startWidget.h"
#include "DrawingPage2.h"

StartWidget::StartWidget(string viewpoint, string object, string system)
: QWidget(Q_NULLPTR), viewpoint_(viewpoint), object_(object)
{
	if (strcmp(system.c_str(), "windows")==0)
		createLayout();
	else
		createLayoutPath();

	mainLayout = new QVBoxLayout;
	mainLayout->addWidget(gridGroupBox);
	setLayout(mainLayout);

	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(255, 255, 255, 255));
	this->setAutoFillBackground(true);
	this->setPalette(pal);
}


StartWidget::~StartWidget()
{
	
	delete button_right;
	delete button_left;
	delete layout;
	delete gridGroupBox;
	delete mainLayout;
}

const char* StartWidget::selectHand(){
    const char* hand;

	if (button_right->isChecked())
		hand = "right";
	else
		hand = "left";
	
	return hand;
}

void StartWidget::openDrawingWindowMain()
{
	DrawingPage2 *newPage;

	if (strcmp(viewpoint_.c_str(), "v1") == 0)
	{
		newPage = new DrawingPage2V1(0, selectHand());
	}
	else
	{
		newPage = new DrawingPage2V2(0, selectHand());
	}
	
	newPage->show();
	close();
}


void StartWidget::openDrawingWindowSecondary()
{
	DrawingPage2 *newPage;

	
	if (strcmp(viewpoint_.c_str(), "v1") == 0)
	{	
		newPage = new DrawingPage2V1(1, selectHand());
	}
	else
	{		
		newPage = new DrawingPage2V2(1, selectHand());
	}
	newPage->show();
	close();
}

void StartWidget::createLayoutPath()
{
	layout = new QGridLayout;
	gridGroupBox = new QGroupBox();

	int row = 0;
	//Path for drawings:
    char path_intsruction[] = "Please specify the path to the 'drawings' folder, "
		"located in the same folder as the current application";

	lalbel_path_instructions = new QLabel(tr(path_intsruction));
	layout->addWidget(lalbel_path_instructions, row++, 0, 1, 1, Qt::AlignCenter);

	select_dir_bttn = new QPushButton(tr("Select..."));
	layout->addWidget(select_dir_bttn, row++, 0, 1, 1, Qt::AlignLeft);
	connect(select_dir_bttn, SIGNAL(clicked()), this, SLOT(selectDir()));

	drawings_dir = "The folder is not selected.";
	selected_folder = new QLabel(tr("Selected folder: ") + drawings_dir);
	layout->addWidget(selected_folder, row++, 0, 1, 1, Qt::AlignLeft);

	next_bttn = new QPushButton(tr("Next"));
	layout->addWidget(next_bttn, row++, 0, 1, 1, Qt::AlignLeft);
	next_bttn->setEnabled(false);
	connect(next_bttn, SIGNAL(clicked()), this, SLOT(createLayout()));
	gridGroupBox->setLayout(layout);

	gridGroupBox->setLayout(layout);
}

void StartWidget::createStartLayout(){
	delete lalbel_path_instructions;
	delete select_dir_bttn;
	delete next_bttn;
	delete selected_folder;
	delete layout;
	delete gridGroupBox;

	createLayout();
}

void StartWidget::createLayout()
{
	gridGroupBox = new QGroupBox();

	layout = new QGridLayout;

	int row = 0;

	//Hand selection.
	button_right = new QRadioButton("Right", this);
	button_left = new QRadioButton("Left", this);

	button_right->setChecked(true);
	
	layout->addWidget(new QLabel(tr("Which hand do you write/draw with?")), row++, 0, 1, 1, Qt::AlignCenter);

	layout->addWidget(button_right, row++, 0, 1, 1, Qt::AlignLeft);
	layout->addWidget(button_left, row++, 0, 1, 1, Qt::AlignLeft);

	
	//Screen selection.
	layout->addWidget(new QLabel(tr("Please select the screen in which to run the application.")), row++, 0, 1, 1, Qt::AlignCenter);
	
	QPushButton *main_screen = new QPushButton(tr("Run in the main screen"));
	connect(main_screen, SIGNAL(clicked()), this, SLOT(openDrawingWindowMain()));
	main_screen->setFixedWidth(300);
	layout->addWidget(main_screen, row++, 0, 1, 1, Qt::AlignCenter);

	QPushButton *second_screen = new QPushButton(tr("Run in the secondary screen"));
	connect(second_screen, SIGNAL(clicked()), this, SLOT(openDrawingWindowSecondary()));
	second_screen->setFixedWidth(300);
	layout->addWidget(second_screen, row++, 0, 1, 1, Qt::AlignCenter);

	gridGroupBox->setLayout(layout);
}

//Mac
void StartWidget::selectDir()
{
	drawings_dir = QFileDialog::getExistingDirectory(this, tr("path to drawings folder"),
		tr("/"), QFileDialog::ShowDirsOnly);
	if (!drawings_dir.isNull())
	{
		selected_folder->setText(tr("Selected folder: ") + drawings_dir);
		next_bttn->setEnabled(true);
	}
}
