#include "DrawingPage2.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/stat.h>
#include <QDir>
#include <QString>

DrawingPage2::DrawingPage2(int screen_number, const char *hand, string drawings_dir, string system)
: system_(system), folder_drawings_(drawings_dir)
{
	

	string title = "Drawing page: ";
	setWindowTitle(tr(title.c_str()));

	recordScreenData(screen_number, hand);
	QRect rec = QApplication::desktop()->screenGeometry(screen_number);
	screen_height = rec.height();
	screen_width = rec.width();

	if (strcmp("right", hand) == 0)
		hand_ = 1;
	else
		hand_ = 0;

	setAlignment();	
	createLayout();
	

	mainLayout = new QVBoxLayout;
	mainLayout->addWidget(gridGroupBox);
	mainLayout->setSpacing(0);
	mainLayout->setContentsMargins(0, 0, 0, 0);
	setLayout(mainLayout);

	this->setGeometry(rec);
	this->setWindowState(Qt::WindowFullScreen);

	QPalette pal = palette();

	pal.setColor(QPalette::Background, QColor(140, 140, 140, 255));
	this->setAutoFillBackground(true);
	this->setPalette(pal);

}

void DrawingPage2::recordScreenData(int screen_number, const char *hand)
{
	string log_filename = folder_drawings_ + "log.json";
	
	QString qfolder_drawings = QString::fromStdString(folder_drawings_);
	if (!QDir(folder_drawings_.c_str()).exists())
		QDir().mkdir(qfolder_drawings);
	
	

	QJsonObject root;
	QJsonArray screensData;
	
	int num_screens = QApplication::desktop()->screenCount();

	for (int i = 0; i < num_screens; i++)
	{
		QRect rec = QApplication::desktop()->screenGeometry(i);
		int screen_height = rec.height();
		int screen_width = rec.width();
		
		QJsonObject screenObject;

		screenObject.insert("screen height", QJsonValue::fromVariant(screen_height));
		screenObject.insert("screen width", QJsonValue::fromVariant(screen_width));
		
		screensData.push_back(screenObject);	
	}

	root.insert("screens data", screensData);
	root.insert("screen drawing number", QJsonValue::fromVariant(screen_number));
	root.insert("hand", QJsonValue::fromVariant(hand));

	QString pVersion = QSysInfo::productVersion();
	root.insert("system version", QJsonValue::fromVariant(pVersion));
	QString pType = QSysInfo::productType();
	root.insert("system type", QJsonValue::fromVariant(pType));
	

	QJsonDocument doc(root);
	QFile jsonFile;
	jsonFile.setFileName(QString(log_filename.c_str()));
	jsonFile.open(QIODevice::WriteOnly | QIODevice::Text);
	jsonFile.write(doc.toJson());
	jsonFile.close();
}

DrawingPage2::~DrawingPage2()
{
	delete initial_drawing;
	delete clean_drawing;
	delete transparency_slider;
	delete completedCleanBtn;
	
	delete completed;
	delete instruction;

	delete canvasTaskText;
	
	delete layout;
	delete gridGroupBox;
	delete mainLayout;
	delete task_;
}

void DrawingPage2::removeReferenceViews()
{
	delete orth_images_slot_;
	delete orth_drawing; 
}


void DrawingPage2::createReferenceViews()
{
	string image_file;

	image_file = ":/orth_views/orth_views.png";

	
	if (!orth_views_.load(QString(image_file.c_str())))
		throw("Unable to load orth_views");

	orth_images_slot_ = new QLabel();
	img_w = double(canvas_dim) / 1660.0 * 560.0;

	orth_images_slot_->setPixmap(orth_views_.scaled(img_w, canvas_dim, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	orth_images_slot_->setStyleSheet("QLabel {margin-left: 10px; margin-right: 10px;}");
	layout->addWidget(orth_images_slot_, 0, 1 - hand_, 3, 1, Qt::AlignTop);
	
	orth_drawing = new TabletCanvas(canvas_dim, img_w);
	orth_drawing->setColor(QColor(0,0,130));
	layout->addWidget(orth_drawing, 0, 1 - hand_, 3, 1, Qt::AlignTop);
	orth_drawing->setFixedSize(img_w*1.2, canvas_dim);

}

void DrawingPage2::setAlignment()
{
	if (hand_)
	{
		flag_canvas = Qt::AlignRight;
		flag_imgs = Qt::AlignLeft;
	}
	else
	{
		flag_canvas = Qt::AlignLeft;
		flag_imgs = Qt::AlignRight;
	}
}

void DrawingPage2::createLayout()
{
	gridGroupBox = new QGroupBox();
	layout = new QGridLayout;

	canvas_dim = 0.9*screen_height;


	//Orthographic views:
	cout << "Create orth views" << endl;
	createReferenceViews();
	cout << "done" << endl;

	//White background:
	QPixmap bgPixmap = QPixmap(canvas_dim, canvas_dim);
	bgPixmap.fill(Qt::white);
	imageBg = new QLabel();
	imageBg->setPixmap(bgPixmap);
	//layout->addWidget(imageBg, 0, hand_, 3, 1, flag_canvas | Qt::AlignTop);
	layout->addWidget(imageBg, 0, hand_, 3, 1, flag_imgs | Qt::AlignTop);


	//drawing canvas
	initial_drawing = new TabletCanvas(canvas_dim, canvas_dim);
	initial_drawing->setFixedSize(canvas_dim, canvas_dim);
	//layout->addWidget(initial_drawing, 0, hand_, 3, 1, flag_canvas | Qt::AlignTop);
	layout->addWidget(initial_drawing, 0, hand_, 3, 1, flag_imgs | Qt::AlignTop);

	createButtonsIntial();

	//layout->addWidget(groupBttns, 4, hand_, 1, 1, flag_canvas);
	layout->addWidget(groupBttns, 4, hand_, 1, 1, flag_imgs);

	gridGroupBox->setLayout(layout);

}

void DrawingPage2::createButtonsIntial()
{
	groupBttns = new QGroupBox();

	qhbox_layout = new QHBoxLayout;

	undoBttn = new QPushButton(tr("Undo"));
	connect(undoBttn, SIGNAL(clicked()), this, SLOT(undoInitial()));
	undoBttn->setFixedWidth(img_w / 2.0);
	undoBttn->setShortcut(QKeySequence::Undo);
	qhbox_layout->addWidget(undoBttn, 0, Qt::AlignLeft);


	completed = new QPushButton(tr("Next task"));
	connect(completed, SIGNAL(clicked()), this, SLOT(handleNextTaskBttn()));
	completed->setFixedWidth(img_w / 2.0);
	qhbox_layout->addWidget(completed, 0, Qt::AlignRight);

	groupBttns->setLayout(qhbox_layout);
	groupBttns->setFixedWidth(canvas_dim);
	groupBttns->setFlat(true);

}

void DrawingPage2::deleteButtonsIntial()
{
	delete undoBttn;
	delete qhbox_layout;

	delete completed;
	delete groupBttns;
}

void DrawingPage2::createButtonsClean(double transparency)
{
	delete undoBttn;
	delete completed;

	undoBttn = new QPushButton(tr("Undo"));
	connect(undoBttn, SIGNAL(clicked()), this, SLOT(undoClean()));
	undoBttn->setFixedWidth(img_w / 2.0);
	undoBttn->setShortcut(QKeySequence::Undo);
	qhbox_layout->addWidget(undoBttn, 0, Qt::AlignLeft);
	

	//Add the transparency slider
	transparency_slider = new QSlider(Qt::Horizontal);
	transparency_slider->setFixedWidth(canvas_dim / 2.0);
	transparency_slider->setMinimum(0);
	transparency_slider->setMaximum(100);
	transparency_slider->setSingleStep(1);
	transparency_slider->setValue(transparency * 100);
	connect(transparency_slider, SIGNAL(valueChanged(int)), this, SLOT(changeTransparency(int)));
	qhbox_layout->addWidget(transparency_slider, 0, Qt::AlignRight);


    completedCleanBtn = new QPushButton(tr("Exit"));
	connect(completedCleanBtn, SIGNAL(clicked()), this, SLOT(done()));
	completedCleanBtn->setFixedWidth(canvas_dim / 6.0);
	qhbox_layout->addWidget(completedCleanBtn, 0, Qt::AlignRight);
	
	groupBttns->setLayout(qhbox_layout);
}


void DrawingPage2::undoInitial()
{
	int n = initial_drawing->drawing->userStrokes.size();

	if (n > 0)
	{
		n --;
		while (n >= 0 && initial_drawing->drawing->userStrokes[n]->is_removed_)
			n --;
	}

	if (n >= 0)
	{
		initial_drawing->drawing->userStrokes[n]->is_removed_ = true;
		initial_drawing->m_pixmap.fill(Qt::transparent);
		initial_drawing->drawing->displayDrawing(initial_drawing->m_pixmap);
		initial_drawing->update();
	}
}

void DrawingPage2::undoClean()
{
	int n = clean_drawing->drawing->userStrokes.size();

	if (n > 0)
	{
		n --;
		while (n >= 0 && clean_drawing->drawing->userStrokes[n]->is_removed_)
			n --;
	}

	if (n >= 0)
	{
		clean_drawing->drawing->userStrokes[n]->is_removed_ = true;
		clean_drawing->m_pixmap.fill(Qt::transparent);
		clean_drawing->drawing->displayDrawing(clean_drawing->m_pixmap);
		clean_drawing->update();
	}
}

void DrawingPage2::handleNextTaskBttn()
{
	

	saveInitialDrawing();
	removeReferenceViews();
	/*delete initial_drawing;
	delete imageBg;
	deleteButtonsIntial();*/

	
	// Instructions:
	const char* instructions_txt = "<h3> Instructions: </h3> <h4>Polished/Presentation sketch </h4>"
        " <p> Using the initial sketch as underlay, create a NEW presentation sketch to communicate the shape to other people."
        " <p> Please try to align the new sketch to the initial one."
		" <p> You can adjust the transparency of the initial drawing using the slider below the drawing area. </p> "
		" <p> Press <b>Exit</b> when you are happy with a result.";

	instruction = new QLabel();

	instruction->setText(instructions_txt);
	instruction->setTextFormat(Qt::RichText);
	instruction->setAlignment(Qt::AlignTop | Qt::AlignLeft);	
	//
	instruction->setStyleSheet("QLabel { background-color : rgb(255,255,255); font: 14px; padding: 20px; margin-left: 10px; margin-right: 10px;}");
	instruction->setWordWrap(true);

	layout->addWidget(instruction, 0, 1 - hand_, 3, 1, Qt::AlignTop);
	instruction->setFixedSize(img_w*1.2, canvas_dim);

	
	canvasTaskText->setText("Polished/Presentation skecth");

	//Display the transparent vertion of an inital drawing:
	initial_drawing->drawing->setColor(QColor(0, 0, 255));
	initial_drawing->drawing->displayDrawing(initial_drawing->m_pixmap);

	double transparency = 0.3;
	initial_drawing_pixmap_ = initial_drawing->m_pixmap;
	QPixmap intial_pixmap_transparent = getPixmapTransparent(initial_drawing_pixmap_, transparency);
	

	initial_drawing->displayPixmap(intial_pixmap_transparent);

	//Prepare a canvas for a clean drwaing:
	clean_drawing = new TabletCanvas(canvas_dim, canvas_dim);
	clean_drawing->setFixedSize(canvas_dim, canvas_dim);
	//layout->addWidget(clean_drawing, 0, hand_, 3, 1, flag_canvas | Qt::AlignTop);
	layout->addWidget(clean_drawing, 0, hand_, 3, 1, flag_imgs | Qt::AlignTop);

	createButtonsClean(transparency);

	//layout->addWidget(groupBttns, 4, hand_, 1, 1, flag_canvas);
	layout->addWidget(groupBttns, 4, hand_, 1, 1, flag_imgs);

	

	//Update the layout:
	gridGroupBox->setLayout(layout);

	delete mainLayout;
	mainLayout = new QVBoxLayout;
	mainLayout->addWidget(gridGroupBox);
	setLayout(mainLayout);
}

void DrawingPage2::changeTransparency(int alpha)
{
	transparency_slider->setValue(alpha);

	QPixmap intial_pixmap_transparent = getPixmapTransparent(initial_drawing_pixmap_, double(alpha) / 100.0);
	initial_drawing->displayPixmap(intial_pixmap_transparent);

}

void DrawingPage2::saveCleanDrawing()
{
	string filename = folder_drawings_ + view_ + "_outline_" + fileVersion_ + ".json";

	clean_drawing->serialize(QString(filename.c_str()));
	filename = folder_drawings_ + view_ + "_outline_" + fileVersion_ + ".png";

	QPixmap clean_drawing_pixmap = clean_drawing->m_pixmap;
	clean_drawing_pixmap.save(QString(filename.c_str()));
}

void DrawingPage2::saveInitialDrawing()
{
	initial_drawing_pixmap_ = initial_drawing->m_pixmap;

	int ind = 0;
	std::stringstream ss;
	ss << std::setw(2) << std::setfill('0') << ind;
	fileVersion_ = ss.str();


	string filename = folder_drawings_ + view_ + "_" + fileVersion_ + ".json";


	while (file_exists(filename))
	{
		std::stringstream ss;
		ss << std::setw(2) << std::setfill('0') << ++ind;
		fileVersion_ = ss.str();

		filename = folder_drawings_ + view_ + "_" + fileVersion_ + ".json";
	}

	initial_drawing->serialize(QString(filename.c_str()));

	filename = folder_drawings_ + view_ + "_" + fileVersion_ + ".png";
	initial_drawing_pixmap_.save(QString(filename.c_str()));

	//Orth_views:
	filename = folder_drawings_ + view_ + "_orth_views_" + fileVersion_ + ".json";

	orth_drawing->serialize(QString(filename.c_str()));
	filename = folder_drawings_ + view_ + "_orth_views_" + fileVersion_ + ".png";

	QPixmap orth_drawing_pixmap = orth_drawing->m_pixmap;
	orth_drawing_pixmap.save(QString(filename.c_str()));

}


inline bool file_exists(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

QPixmap DrawingPage2::getPixmapTransparent(const QPixmap input, double alpha)
{
	QImage image(input.width(), input.height(), QImage::Format_ARGB32_Premultiplied);

	image.fill(Qt::transparent);
	QPainter p(&image);
	p.setCompositionMode(QPainter::CompositionMode_Source);
	p.setOpacity(alpha);
	p.drawPixmap(0, 0, input);
	p.end();

	return QPixmap::fromImage(image);
}

void DrawingPage2::done()
{
	saveCleanDrawing();
	
    QMessageBox msgBox;
    msgBox.setText("Thank you! Your work is saved. If you are done with both views, please upload the drawings folder content using the online interface.");
    //msgBox.setInformativeText("Save an exit?");
    msgBox.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);

    int ret = msgBox.exec();

    switch (ret) {
      case QMessageBox::Ok:
          close();
          break;
      case QMessageBox::Cancel:
          // Cancel was clicked
          break;
      default:
          // should never be reached
          break;
    }

	return;
}





DrawingPage2V1::DrawingPage2V1(int screen_number, const char *hand)
: DrawingPage2(screen_number, hand)
{
	view_ = "v1";
	task_ = "3/4 bird's eye perspective view";

	//canvas label task
	canvasTaskText = new QLabel(tr(task_));
	canvasTaskText->setStyleSheet("QLabel { color : grey; font: 14px; padding: 10px; }");
//	layout->addWidget(canvasTaskText, 0, hand_, 1, 1, flag_canvas | Qt::AlignTop);
	layout->addWidget(canvasTaskText, 0, hand_, 1, 1, flag_imgs | Qt::AlignTop);

	//Update the layout:
	gridGroupBox->setLayout(layout);

	delete mainLayout;
	mainLayout = new QVBoxLayout;
	mainLayout->addWidget(gridGroupBox);
	setLayout(mainLayout);
}

DrawingPage2V2::DrawingPage2V2(int screen_number, const char *hand)
: DrawingPage2(screen_number, hand)
{
	view_ = "v2";
	task_ = "Perspective view of free choice";

	//canvas label task
	canvasTaskText = new QLabel(tr(task_));
	canvasTaskText->setStyleSheet("QLabel { color : grey; font: 14px; padding: 10px; }");
	//layout->addWidget(canvasTaskText, 0, hand_, 1, 1, flag_canvas | Qt::AlignTop);
	layout->addWidget(canvasTaskText, 0, hand_, 1, 1, flag_imgs | Qt::AlignTop);

	//Update the layout:
	gridGroupBox->setLayout(layout);

	delete mainLayout;
	mainLayout = new QVBoxLayout;
	mainLayout->addWidget(gridGroupBox);
	setLayout(mainLayout);
}
