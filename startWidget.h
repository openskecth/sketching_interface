#ifndef STARTWIDGET_H
#define STARTWIDGET_H

#include <QWidget>
#include <QtWidgets>
#include <string>
using namespace std;

class QGroupBox;
class QAction;


class StartWidget : public QWidget
{
	Q_OBJECT
private:
	void createLayout(); //windows
	void createLayoutPath(); //mac
	string object_; //for mac

	const char* selectHand();
	string viewpoint_;

	QGroupBox *gridGroupBox;
	QGridLayout *layout;
	QVBoxLayout *mainLayout;
	QRadioButton *button_right;
	QRadioButton *button_left;

	
	
	QString drawings_dir; // Mac:

	//First Layout
	QLabel* lalbel_path_instructions; // Mac:
	QPushButton* select_dir_bttn;// Mac:
	QPushButton* next_bttn;// Mac:
	QLabel* selected_folder;// Mac:
public:
	StartWidget(string viewpoint, string object = "undefined", string system = "windows");
	~StartWidget();

private slots:
	void openDrawingWindowMain();
	void openDrawingWindowSecondary();

	void selectDir(); //Mac
	void createStartLayout(); //Mac
};

#endif
