There are a few things one might want to change.
The strokes that were removed are stores, but to make it fully useful one might want to add the time when the strokes is removed.

I also stored the points of the strokes for the cases when the stroke was originated within the canvas but then continued out of it. 
If you do not want that at serialization step you could add a check that the point is still within canvas boundaries. 

You can compile it either for windows or mac. 
For this you need to change “system” variable in startWidget.cpp.
This would handelt differently file systems. In case of windows it is possible to get a relative path, while in case of Mac one needs to specify the path, where to save the recorded data.

StartWidget::StartWidget(string viewpoint, string object, string system)
    : QWidget(Q_NULLPTR), viewpoint_(viewpoint), object_(object)
if (strcmp(system.c_str(), "windows")==0)
    createLayout();
else
	createLayoutPath();


In tabletcanvas.h serialize() writes to a JSON file information about each stroke point.

void serialize(QString filename); //Write to JSON file
bool saveImage(const QString &file); // Saves both the image from the canvas as png and writes to JSON.

It saves to the drawings folder inside the folder from which the application is called, or the one specified in case of Mac.

In
void TabletCanvas::tabletEvent(QTabletEvent *event)
The rotation of a styles is obtained, but is not used in this version:
lastPoint.rotation = event->rotation();

## Build
To build the projetc simply run CMAKE. You will need to change QTDIR value.

## Deploy Mac or Windows
For deployment on windows:
add_custom_command(TARGET QtDrawingInterface POST_BUILD
    COMMAND "${CMAKE_COMMAND}" -E
        env PATH="${_qt_bin_dir}" "${WINDEPLOYQT_EXECUTABLE}"
            "$<TARGET_FILE:QtDrawingInterface>"
    COMMENT "Running windeployqt..."
)

For deployment on Mac:
Mac:
 add_custom_command(TARGET QtDrawingInterface POST_BUILD
    COMMAND "${MACDEPLOYQT_EXECUTABLE}"
        "$<TARGET_FILE_DIR:QtDrawingInterface>/../.."
        -always-overwrite
    COMMENT "Running macdeployqt..."
)

## This code is a part of OpenSketch publication
Project page:
https://ns.inria.fr/d3/OpenSketch/

Additional data:
https://repo-sam.inria.fr/d3/OpenSketch/


## Contact

If you have any questions please contact Yulia Gryaditskaya yulia.gryaditskaya@gmail.com

